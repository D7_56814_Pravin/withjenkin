const express=require(`express`)
const db=require('../db')
const utils=require('../utils')
const cryptoJs=require('crypto-js')
const e=require('express')
const { response, request, Router } = require('express')
const { CLIENT_SECURE_CONNECTION } = require('mysql/lib/protocol/constants/client')
const { route } = require('express/lib/application')


const router=express.Router()

router.post('/',(request,response)=>{
    const{name,password,email}=request.body
    const connection=db.openConnection()
    const statement =`insert into user(name,password,email) values ('${name}','${password}','${email}')`
    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))
    })
})
 
router.get('/',(request,response)=>{
    const connection=db.openConnection()
    const statement=`select * from user`
    connection.query(statement,(error,result)=>{
        if(error)
        {
            response.send(utils.createResult(error))
        }
        else if(result.length==0)
        {
            response.send(utils.createResult('user not found'))
        }
        else 
        {
            connection.end()
            response.send(utils.createResult(null,result))
        }
    })
})

 router.put('/:id',(request,response)=>{
     const{name,password}=request.body
     const{id}=request.params
     const connection=db.openConnection()
     const statement=`update user set name='${name}',password='${password}'`
     connection.query(statement,(error,result)=>{
         connection.end()
         response.send(utils.createResult(error,result))
     })
 })

 router.delete('/:id',(request,response)=>{
     const{id}=request.params
     const connection=db.openConnection()
     const statement=`delete from user where id='${id}'`
     connection.query(statement,(error,result)=>{
         connection.end()
         response.send(utils.createResult(error,result))
     })
 })

module.exports=router